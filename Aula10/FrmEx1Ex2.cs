﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula10
{
    public partial class FrmEx1Ex2 : Form
    {
        public FrmEx1Ex2()
        {
            InitializeComponent();
        }

        private void FrmEx1Ex2_Load(object sender, EventArgs e)
        {

        }

        private int calcularPotencia(int vBase, int vExpoente)
        {
            int res = 1, i;
            for (i = 1; i <= vExpoente; i++)
            {
                res *= vBase;
            }
            return res;
        }

        private int calcularFatorial(int valor)
        {
            int i, fat = 1;
            for (i = valor; i >= 1; i--)
            {
                fat *= i;
            }
            return fat;
        }

        private void btnCalcular_Click(object sender, EventArgs e)
        {
            int b, ex;
            if (txtBase.Text != "" && txtExpoente.Text != "")
            {
                b = int.Parse(txtBase.Text);
                ex = int.Parse(txtExpoente.Text);
                txtResultado.Text = calcularPotencia(b, ex).ToString();
            }
        }

        private void btnCalcular1_Click(object sender, EventArgs e)
        {
            int valor;
            if (txtValor.Text != "")
            {
                valor = int.Parse(txtValor.Text);
                txtResFat.Text = calcularFatorial(valor).ToString();
            }
        }
    }
}
