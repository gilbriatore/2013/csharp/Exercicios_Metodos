﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Aula10
{
    public partial class frmFolha : Form
    {
        public frmFolha()
        {
            InitializeComponent();
        }

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            /*
             * Ao clicar no botão 'Adicionar folha' o sistema deverá:
             * 1 - Verificar se os campos estão preenchidos
             * 2 - Se os campos estiverem preenchidos adicioná-los no grid (Testar a validade dos campos numéricos)
             * 3 - Habilitar o botão 'Nova folha'
             * 4 - Desabilitar o botão 'Adicionar folha'
             * 5 - Se os campos não estiverem preenchidos, apresentar a mensagem "Preencha todos os campos"
             * 6 - OBS: NÃO PERMITIR FOLHAS COM O MESMO FUNCIONÁRIO, MÊS E ANO
            */
            int horas, mes, ano;
            float valor;
            if (txtFuncionario.Text != "" && txtMes.Text != "" && txtAno.Text != "" && txtHoras.Text != "" && txtValor.Text != "")
            {
                try
                {
                    mes = int.Parse(txtMes.Text);
                    ano = int.Parse(txtAno.Text);
                    horas = int.Parse(txtHoras.Text);
                    valor = float.Parse(txtValor.Text);
                    if (obterFolha(txtFuncionario.Text, mes, ano) == -1)
                    {
                        grdFolhas.Rows.Add(txtFuncionario.Text, mes.ToString(), ano.ToString(), horas.ToString(), valor.ToString("N2"));
                        btnNovo.Enabled = true;
                        btnAdicionar.Enabled = false;
                        btnNovo.Focus();
                    }
                    else
                    {
                        MessageBox.Show("Folha já cadastrada", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        txtFuncionario.Focus();
                    }
                }
                catch (Exception error)
                {
                    MessageBox.Show("Valores inválidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtMes.Focus();
                }
            }
            else
            {
                MessageBox.Show("Preencha todos os campos", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtFuncionario.Focus();
            }
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            /*
             * Ao clicar no botão 'Nova folha' o sistema deverá:
             * 1 - Limpar os campos de entrada
             * 2 - Posicionar o foco no campo funcionário
             * 3 - Habilitar o botão 'Adicionar folha'
             * 4 - Desabilitar o botão 'Nova folha'             
            */
            txtFuncionario.Clear();
            txtMes.Clear();
            txtAno.Clear();
            txtHoras.Clear();
            txtValor.Clear();
            btnAdicionar.Enabled = true;
            btnNovo.Enabled = false;
            txtFuncionario.Focus();
        }

        private void grdFolhas_DoubleClick(object sender, EventArgs e)
        {
            /*
             * Ao clicar duas vezes em uma respectiva folha (linha no grid) o sistema deverá:
             * 1 - Calcular e mostrar: O funcionário selecionado, salário bruto, inss, ir, fgts e salário liquido
             * OBS: Os cálculos estão no documento PROVA.PDF
             * 2 - Verificar se a linha selecionada é válida. Se não for, exibir a mensagem "Folha inválida."             
            */
            float valor, bruto;
            int horas;
            try
            {
                txtFuncionarioSelecionado.Text = grdFolhas.CurrentRow.Cells["grdFuncionario"].Value.ToString();
                horas = int.Parse(grdFolhas.CurrentRow.Cells["grdHoras"].Value.ToString());
                valor = float.Parse(grdFolhas.CurrentRow.Cells["grdValor"].Value.ToString());
                bruto = FolhaMetodos.calcularSalarioBruto(horas, valor);
                txtBruto.Text = bruto.ToString("N2");
                txtInss.Text = FolhaMetodos.calcularInss(bruto).ToString("N2");
                txtIr.Text = FolhaMetodos.calcularIr(bruto).ToString("N2");
                txtFgts.Text = FolhaMetodos.calcularFgts(bruto).ToString("N2");
                txtLiquido.Text = FolhaMetodos.calcularSalarioLiquido(bruto).ToString("N2");
            }
            catch (Exception error)
            {
                MessageBox.Show("Folha inválida", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnTotalizar_Click(object sender, EventArgs e)
        {
            /*
             * Ao clicar no botão 'Totalizar' o sistema deverá:
             * 1 - Verificar se os campos mês e ano estão preenchidos
             * 2 - Apresentar a soma dos salários líquidos para o mês e ano informados
             * 3 - Se os campos não estiverem devidamente preenchidos, apresentar a mensagem "Preencher os campos mês e ano"             
            */
            int i, horas;
            float bruto, valor, liquido = 0, inss, ir;
            if (txtMesTotais.Text != "" && txtAnoTotais.Text != "")
            {
                for (i = 0; i < grdFolhas.Rows.Count - 1; i++)
                {
                    if (grdFolhas.Rows[i].Cells["grdMes"].Value.ToString() == txtMesTotais.Text && grdFolhas.Rows[i].Cells["grdAno"].Value.ToString() == txtAnoTotais.Text)
                    {
                        horas = int.Parse(grdFolhas.Rows[i].Cells["grdHoras"].Value.ToString());
                        valor = float.Parse(grdFolhas.Rows[i].Cells["grdValor"].Value.ToString());
                        bruto = FolhaMetodos.calcularSalarioBruto(horas, valor);
                        inss = FolhaMetodos.calcularInss(bruto);
                        ir = FolhaMetodos.calcularIr(bruto);
                        liquido += FolhaMetodos.calcularSalarioLiquido(bruto);
                    }
                }
                txtLiquidoTotal.Text = liquido.ToString("N2");
            }
            else
            {
                MessageBox.Show("Preencher os campos mês e ano", "Alerta", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                txtMesTotais.Focus();
            }
        }

        private void btnSobre_Click(object sender, EventArgs e)
        {
            /*
             * Ao clicar no botão 'Sobre' o sistema deverá:
             * 1 - Montar uma MessageBox() com o seu nome completo
             */
        }

        private int obterFolha(String funcionario, int mes, int ano)
        {
            int i;
            for (i = 0; i < grdFolhas.Rows.Count - 1; i++)
            {
                if (grdFolhas.Rows[i].Cells["grdFuncionario"].Value.ToString() == funcionario && grdFolhas.Rows[i].Cells["grdMes"].Value.ToString() == mes.ToString() && grdFolhas.Rows[i].Cells["grdAno"].Value.ToString() == ano.ToString())
                {
                    return i;
                }
            }
            return -1;
        }
    }
}
