﻿namespace Aula10
{
    partial class frmFolha
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.grpDados = new System.Windows.Forms.GroupBox();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.txtHoras = new System.Windows.Forms.TextBox();
            this.lblHoras = new System.Windows.Forms.Label();
            this.txtAno = new System.Windows.Forms.TextBox();
            this.lblAno = new System.Windows.Forms.Label();
            this.txtMes = new System.Windows.Forms.TextBox();
            this.lblMes = new System.Windows.Forms.Label();
            this.txtFuncionario = new System.Windows.Forms.TextBox();
            this.lblFuncionario = new System.Windows.Forms.Label();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.btnNovo = new System.Windows.Forms.Button();
            this.grpfolhas = new System.Windows.Forms.GroupBox();
            this.grdFolhas = new System.Windows.Forms.DataGridView();
            this.grdFuncionario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdMes = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdAno = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdHoras = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grdValor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.grpSalario = new System.Windows.Forms.GroupBox();
            this.txtLiquido = new System.Windows.Forms.TextBox();
            this.lblLiquido = new System.Windows.Forms.Label();
            this.txtFgts = new System.Windows.Forms.TextBox();
            this.lblFgts = new System.Windows.Forms.Label();
            this.txtIr = new System.Windows.Forms.TextBox();
            this.lblIr = new System.Windows.Forms.Label();
            this.txtInss = new System.Windows.Forms.TextBox();
            this.lblInss = new System.Windows.Forms.Label();
            this.txtBruto = new System.Windows.Forms.TextBox();
            this.lblBruto = new System.Windows.Forms.Label();
            this.txtFuncionarioSelecionado = new System.Windows.Forms.TextBox();
            this.lblFuncionarioSelecionado = new System.Windows.Forms.Label();
            this.grpTotais = new System.Windows.Forms.GroupBox();
            this.txtLiquidoTotal = new System.Windows.Forms.TextBox();
            this.lblLiquidoTotal = new System.Windows.Forms.Label();
            this.btnTotalizar = new System.Windows.Forms.Button();
            this.txtAnoTotais = new System.Windows.Forms.TextBox();
            this.lblAnoTotais = new System.Windows.Forms.Label();
            this.txtMesTotais = new System.Windows.Forms.TextBox();
            this.lblMesTotais = new System.Windows.Forms.Label();
            this.btnSobre = new System.Windows.Forms.Button();
            this.grpDados.SuspendLayout();
            this.grpfolhas.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdFolhas)).BeginInit();
            this.grpSalario.SuspendLayout();
            this.grpTotais.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDados
            // 
            this.grpDados.Controls.Add(this.txtValor);
            this.grpDados.Controls.Add(this.lblValor);
            this.grpDados.Controls.Add(this.txtHoras);
            this.grpDados.Controls.Add(this.lblHoras);
            this.grpDados.Controls.Add(this.txtAno);
            this.grpDados.Controls.Add(this.lblAno);
            this.grpDados.Controls.Add(this.txtMes);
            this.grpDados.Controls.Add(this.lblMes);
            this.grpDados.Controls.Add(this.txtFuncionario);
            this.grpDados.Controls.Add(this.lblFuncionario);
            this.grpDados.Location = new System.Drawing.Point(12, 16);
            this.grpDados.Name = "grpDados";
            this.grpDados.Size = new System.Drawing.Size(540, 92);
            this.grpDados.TabIndex = 0;
            this.grpDados.TabStop = false;
            this.grpDados.Text = "Dados da folha";
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(472, 52);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(52, 20);
            this.txtValor.TabIndex = 9;
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValor
            // 
            this.lblValor.Location = new System.Drawing.Point(400, 52);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(100, 20);
            this.lblValor.TabIndex = 8;
            this.lblValor.Text = "Valor da hora";
            this.lblValor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtHoras
            // 
            this.txtHoras.Location = new System.Drawing.Point(336, 52);
            this.txtHoras.Name = "txtHoras";
            this.txtHoras.Size = new System.Drawing.Size(52, 20);
            this.txtHoras.TabIndex = 7;
            this.txtHoras.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblHoras
            // 
            this.lblHoras.Location = new System.Drawing.Point(240, 52);
            this.lblHoras.Name = "lblHoras";
            this.lblHoras.Size = new System.Drawing.Size(100, 20);
            this.lblHoras.TabIndex = 6;
            this.lblHoras.Text = "Horas trabalhadas";
            this.lblHoras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtAno
            // 
            this.txtAno.Location = new System.Drawing.Point(176, 52);
            this.txtAno.Name = "txtAno";
            this.txtAno.Size = new System.Drawing.Size(52, 20);
            this.txtAno.TabIndex = 5;
            this.txtAno.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAno
            // 
            this.lblAno.Location = new System.Drawing.Point(140, 52);
            this.lblAno.Name = "lblAno";
            this.lblAno.Size = new System.Drawing.Size(64, 20);
            this.lblAno.TabIndex = 4;
            this.lblAno.Text = "Ano";
            this.lblAno.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMes
            // 
            this.txtMes.Location = new System.Drawing.Point(80, 52);
            this.txtMes.Name = "txtMes";
            this.txtMes.Size = new System.Drawing.Size(52, 20);
            this.txtMes.TabIndex = 3;
            this.txtMes.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMes
            // 
            this.lblMes.Location = new System.Drawing.Point(12, 52);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(64, 20);
            this.lblMes.TabIndex = 2;
            this.lblMes.Text = "Mês";
            this.lblMes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuncionario
            // 
            this.txtFuncionario.Location = new System.Drawing.Point(80, 24);
            this.txtFuncionario.Name = "txtFuncionario";
            this.txtFuncionario.Size = new System.Drawing.Size(444, 20);
            this.txtFuncionario.TabIndex = 1;
            // 
            // lblFuncionario
            // 
            this.lblFuncionario.Location = new System.Drawing.Point(12, 24);
            this.lblFuncionario.Name = "lblFuncionario";
            this.lblFuncionario.Size = new System.Drawing.Size(100, 20);
            this.lblFuncionario.TabIndex = 0;
            this.lblFuncionario.Text = "Funcionário";
            this.lblFuncionario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.Location = new System.Drawing.Point(564, 28);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(136, 28);
            this.btnAdicionar.TabIndex = 1;
            this.btnAdicionar.Text = "&Adicionar folha";
            this.btnAdicionar.UseVisualStyleBackColor = true;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // btnNovo
            // 
            this.btnNovo.Enabled = false;
            this.btnNovo.Location = new System.Drawing.Point(564, 64);
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(136, 28);
            this.btnNovo.TabIndex = 2;
            this.btnNovo.Text = "&Nova folha";
            this.btnNovo.UseVisualStyleBackColor = true;
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // grpfolhas
            // 
            this.grpfolhas.Controls.Add(this.grdFolhas);
            this.grpfolhas.Location = new System.Drawing.Point(12, 116);
            this.grpfolhas.Name = "grpfolhas";
            this.grpfolhas.Size = new System.Drawing.Size(688, 276);
            this.grpfolhas.TabIndex = 3;
            this.grpfolhas.TabStop = false;
            this.grpfolhas.Text = "Folhas cadastradas";
            // 
            // grdFolhas
            // 
            this.grdFolhas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.grdFolhas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.grdFuncionario,
            this.grdMes,
            this.grdAno,
            this.grdHoras,
            this.grdValor});
            this.grdFolhas.Location = new System.Drawing.Point(8, 20);
            this.grdFolhas.MultiSelect = false;
            this.grdFolhas.Name = "grdFolhas";
            this.grdFolhas.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdFolhas.Size = new System.Drawing.Size(668, 240);
            this.grdFolhas.TabIndex = 0;
            this.grdFolhas.TabStop = false;
            this.grdFolhas.DoubleClick += new System.EventHandler(this.grdFolhas_DoubleClick);
            // 
            // grdFuncionario
            // 
            this.grdFuncionario.HeaderText = "Funcionário";
            this.grdFuncionario.Name = "grdFuncionario";
            this.grdFuncionario.ReadOnly = true;
            this.grdFuncionario.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.grdFuncionario.Width = 200;
            // 
            // grdMes
            // 
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdMes.DefaultCellStyle = dataGridViewCellStyle1;
            this.grdMes.HeaderText = "Mês";
            this.grdMes.Name = "grdMes";
            this.grdMes.ReadOnly = true;
            this.grdMes.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // grdAno
            // 
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdAno.DefaultCellStyle = dataGridViewCellStyle2;
            this.grdAno.HeaderText = "Ano";
            this.grdAno.Name = "grdAno";
            this.grdAno.ReadOnly = true;
            this.grdAno.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // grdHoras
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdHoras.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdHoras.HeaderText = "Horas trabalhadas";
            this.grdHoras.Name = "grdHoras";
            this.grdHoras.ReadOnly = true;
            this.grdHoras.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // grdValor
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.grdValor.DefaultCellStyle = dataGridViewCellStyle4;
            this.grdValor.HeaderText = "Valor da hora";
            this.grdValor.Name = "grdValor";
            this.grdValor.ReadOnly = true;
            this.grdValor.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // grpSalario
            // 
            this.grpSalario.Controls.Add(this.txtLiquido);
            this.grpSalario.Controls.Add(this.lblLiquido);
            this.grpSalario.Controls.Add(this.txtFgts);
            this.grpSalario.Controls.Add(this.lblFgts);
            this.grpSalario.Controls.Add(this.txtIr);
            this.grpSalario.Controls.Add(this.lblIr);
            this.grpSalario.Controls.Add(this.txtInss);
            this.grpSalario.Controls.Add(this.lblInss);
            this.grpSalario.Controls.Add(this.txtBruto);
            this.grpSalario.Controls.Add(this.lblBruto);
            this.grpSalario.Controls.Add(this.txtFuncionarioSelecionado);
            this.grpSalario.Controls.Add(this.lblFuncionarioSelecionado);
            this.grpSalario.Location = new System.Drawing.Point(16, 400);
            this.grpSalario.Name = "grpSalario";
            this.grpSalario.Size = new System.Drawing.Size(684, 96);
            this.grpSalario.TabIndex = 4;
            this.grpSalario.TabStop = false;
            this.grpSalario.Text = "Cálculo do salário";
            // 
            // txtLiquido
            // 
            this.txtLiquido.Location = new System.Drawing.Point(568, 48);
            this.txtLiquido.Name = "txtLiquido";
            this.txtLiquido.ReadOnly = true;
            this.txtLiquido.Size = new System.Drawing.Size(104, 20);
            this.txtLiquido.TabIndex = 13;
            this.txtLiquido.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLiquido
            // 
            this.lblLiquido.Location = new System.Drawing.Point(496, 48);
            this.lblLiquido.Name = "lblLiquido";
            this.lblLiquido.Size = new System.Drawing.Size(80, 20);
            this.lblLiquido.TabIndex = 12;
            this.lblLiquido.Text = "Salário líquido";
            this.lblLiquido.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFgts
            // 
            this.txtFgts.Location = new System.Drawing.Point(388, 52);
            this.txtFgts.Name = "txtFgts";
            this.txtFgts.ReadOnly = true;
            this.txtFgts.Size = new System.Drawing.Size(100, 20);
            this.txtFgts.TabIndex = 11;
            this.txtFgts.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblFgts
            // 
            this.lblFgts.Location = new System.Drawing.Point(348, 52);
            this.lblFgts.Name = "lblFgts";
            this.lblFgts.Size = new System.Drawing.Size(44, 20);
            this.lblFgts.TabIndex = 10;
            this.lblFgts.Text = "FGTS";
            this.lblFgts.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtIr
            // 
            this.txtIr.Location = new System.Drawing.Point(232, 52);
            this.txtIr.Name = "txtIr";
            this.txtIr.ReadOnly = true;
            this.txtIr.Size = new System.Drawing.Size(108, 20);
            this.txtIr.TabIndex = 9;
            this.txtIr.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblIr
            // 
            this.lblIr.Location = new System.Drawing.Point(200, 52);
            this.lblIr.Name = "lblIr";
            this.lblIr.Size = new System.Drawing.Size(44, 20);
            this.lblIr.TabIndex = 8;
            this.lblIr.Text = "IR";
            this.lblIr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtInss
            // 
            this.txtInss.Location = new System.Drawing.Point(76, 52);
            this.txtInss.Name = "txtInss";
            this.txtInss.ReadOnly = true;
            this.txtInss.Size = new System.Drawing.Size(108, 20);
            this.txtInss.TabIndex = 7;
            this.txtInss.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblInss
            // 
            this.lblInss.Location = new System.Drawing.Point(8, 52);
            this.lblInss.Name = "lblInss";
            this.lblInss.Size = new System.Drawing.Size(72, 20);
            this.lblInss.TabIndex = 6;
            this.lblInss.Text = "INSS";
            this.lblInss.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBruto
            // 
            this.txtBruto.Location = new System.Drawing.Point(568, 20);
            this.txtBruto.Name = "txtBruto";
            this.txtBruto.ReadOnly = true;
            this.txtBruto.Size = new System.Drawing.Size(104, 20);
            this.txtBruto.TabIndex = 5;
            this.txtBruto.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBruto
            // 
            this.lblBruto.Location = new System.Drawing.Point(496, 20);
            this.lblBruto.Name = "lblBruto";
            this.lblBruto.Size = new System.Drawing.Size(72, 20);
            this.lblBruto.TabIndex = 4;
            this.lblBruto.Text = "Salário bruto";
            this.lblBruto.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtFuncionarioSelecionado
            // 
            this.txtFuncionarioSelecionado.Location = new System.Drawing.Point(76, 24);
            this.txtFuncionarioSelecionado.Name = "txtFuncionarioSelecionado";
            this.txtFuncionarioSelecionado.ReadOnly = true;
            this.txtFuncionarioSelecionado.Size = new System.Drawing.Size(412, 20);
            this.txtFuncionarioSelecionado.TabIndex = 3;
            // 
            // lblFuncionarioSelecionado
            // 
            this.lblFuncionarioSelecionado.Location = new System.Drawing.Point(8, 24);
            this.lblFuncionarioSelecionado.Name = "lblFuncionarioSelecionado";
            this.lblFuncionarioSelecionado.Size = new System.Drawing.Size(100, 20);
            this.lblFuncionarioSelecionado.TabIndex = 2;
            this.lblFuncionarioSelecionado.Text = "Funcionário";
            this.lblFuncionarioSelecionado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // grpTotais
            // 
            this.grpTotais.Controls.Add(this.txtLiquidoTotal);
            this.grpTotais.Controls.Add(this.lblLiquidoTotal);
            this.grpTotais.Controls.Add(this.btnTotalizar);
            this.grpTotais.Controls.Add(this.txtAnoTotais);
            this.grpTotais.Controls.Add(this.lblAnoTotais);
            this.grpTotais.Controls.Add(this.txtMesTotais);
            this.grpTotais.Controls.Add(this.lblMesTotais);
            this.grpTotais.Location = new System.Drawing.Point(16, 504);
            this.grpTotais.Name = "grpTotais";
            this.grpTotais.Size = new System.Drawing.Size(684, 64);
            this.grpTotais.TabIndex = 6;
            this.grpTotais.TabStop = false;
            this.grpTotais.Text = "Totais";
            // 
            // txtLiquidoTotal
            // 
            this.txtLiquidoTotal.Location = new System.Drawing.Point(564, 16);
            this.txtLiquidoTotal.Name = "txtLiquidoTotal";
            this.txtLiquidoTotal.ReadOnly = true;
            this.txtLiquidoTotal.Size = new System.Drawing.Size(104, 20);
            this.txtLiquidoTotal.TabIndex = 15;
            this.txtLiquidoTotal.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblLiquidoTotal
            // 
            this.lblLiquidoTotal.Location = new System.Drawing.Point(384, 20);
            this.lblLiquidoTotal.Name = "lblLiquidoTotal";
            this.lblLiquidoTotal.Size = new System.Drawing.Size(176, 32);
            this.lblLiquidoTotal.TabIndex = 14;
            this.lblLiquidoTotal.Text = "Total do salário líquido para o mês e ano informados";
            this.lblLiquidoTotal.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnTotalizar
            // 
            this.btnTotalizar.Location = new System.Drawing.Point(236, 16);
            this.btnTotalizar.Name = "btnTotalizar";
            this.btnTotalizar.Size = new System.Drawing.Size(136, 28);
            this.btnTotalizar.TabIndex = 10;
            this.btnTotalizar.Text = "&Totalizar";
            this.btnTotalizar.UseVisualStyleBackColor = true;
            this.btnTotalizar.Click += new System.EventHandler(this.btnTotalizar_Click);
            // 
            // txtAnoTotais
            // 
            this.txtAnoTotais.Location = new System.Drawing.Point(172, 20);
            this.txtAnoTotais.Name = "txtAnoTotais";
            this.txtAnoTotais.Size = new System.Drawing.Size(52, 20);
            this.txtAnoTotais.TabIndex = 9;
            this.txtAnoTotais.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblAnoTotais
            // 
            this.lblAnoTotais.Location = new System.Drawing.Point(136, 20);
            this.lblAnoTotais.Name = "lblAnoTotais";
            this.lblAnoTotais.Size = new System.Drawing.Size(64, 20);
            this.lblAnoTotais.TabIndex = 8;
            this.lblAnoTotais.Text = "Ano";
            this.lblAnoTotais.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtMesTotais
            // 
            this.txtMesTotais.Location = new System.Drawing.Point(76, 20);
            this.txtMesTotais.Name = "txtMesTotais";
            this.txtMesTotais.Size = new System.Drawing.Size(52, 20);
            this.txtMesTotais.TabIndex = 7;
            this.txtMesTotais.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblMesTotais
            // 
            this.lblMesTotais.Location = new System.Drawing.Point(8, 20);
            this.lblMesTotais.Name = "lblMesTotais";
            this.lblMesTotais.Size = new System.Drawing.Size(64, 20);
            this.lblMesTotais.TabIndex = 6;
            this.lblMesTotais.Text = "Mês";
            this.lblMesTotais.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnSobre
            // 
            this.btnSobre.Location = new System.Drawing.Point(564, 580);
            this.btnSobre.Name = "btnSobre";
            this.btnSobre.Size = new System.Drawing.Size(136, 28);
            this.btnSobre.TabIndex = 11;
            this.btnSobre.Text = "&Sobre";
            this.btnSobre.UseVisualStyleBackColor = true;
            this.btnSobre.Click += new System.EventHandler(this.btnSobre_Click);
            // 
            // frmFolha
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(714, 625);
            this.Controls.Add(this.btnSobre);
            this.Controls.Add(this.grpTotais);
            this.Controls.Add(this.grpSalario);
            this.Controls.Add(this.grpfolhas);
            this.Controls.Add(this.btnNovo);
            this.Controls.Add(this.btnAdicionar);
            this.Controls.Add(this.grpDados);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmFolha";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Folha de Pagamento";
            this.grpDados.ResumeLayout(false);
            this.grpDados.PerformLayout();
            this.grpfolhas.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdFolhas)).EndInit();
            this.grpSalario.ResumeLayout(false);
            this.grpSalario.PerformLayout();
            this.grpTotais.ResumeLayout(false);
            this.grpTotais.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDados;
        private System.Windows.Forms.TextBox txtFuncionario;
        private System.Windows.Forms.Label lblFuncionario;
        private System.Windows.Forms.TextBox txtMes;
        private System.Windows.Forms.Label lblMes;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.TextBox txtHoras;
        private System.Windows.Forms.Label lblHoras;
        private System.Windows.Forms.TextBox txtAno;
        private System.Windows.Forms.Label lblAno;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Button btnNovo;
        private System.Windows.Forms.GroupBox grpfolhas;
        private System.Windows.Forms.DataGridView grdFolhas;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdFuncionario;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdMes;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdAno;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdHoras;
        private System.Windows.Forms.DataGridViewTextBoxColumn grdValor;
        private System.Windows.Forms.GroupBox grpSalario;
        private System.Windows.Forms.TextBox txtFuncionarioSelecionado;
        private System.Windows.Forms.Label lblFuncionarioSelecionado;
        private System.Windows.Forms.TextBox txtLiquido;
        private System.Windows.Forms.Label lblLiquido;
        private System.Windows.Forms.TextBox txtFgts;
        private System.Windows.Forms.Label lblFgts;
        private System.Windows.Forms.TextBox txtIr;
        private System.Windows.Forms.Label lblIr;
        private System.Windows.Forms.TextBox txtInss;
        private System.Windows.Forms.Label lblInss;
        private System.Windows.Forms.TextBox txtBruto;
        private System.Windows.Forms.Label lblBruto;
        private System.Windows.Forms.GroupBox grpTotais;
        private System.Windows.Forms.TextBox txtLiquidoTotal;
        private System.Windows.Forms.Label lblLiquidoTotal;
        private System.Windows.Forms.Button btnTotalizar;
        private System.Windows.Forms.TextBox txtAnoTotais;
        private System.Windows.Forms.Label lblAnoTotais;
        private System.Windows.Forms.TextBox txtMesTotais;
        private System.Windows.Forms.Label lblMesTotais;
        private System.Windows.Forms.Button btnSobre;
    }
}

