﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Aula10
{
    class FolhaMetodos
    {
        public static float calcularSalarioBruto(int horas, float valor)
        {
            return horas * valor;
        }

        public static float calcularIr(float bruto)
        {
            if (bruto <= 1372.81)
            {
                return 0;
            }
            else
            {
                if (bruto <= 2743.25)
                {
                    return (float)((bruto * 15 / 100) - 205.92);
                }
                else
                {
                    return (float)((bruto * 27.5/ 100) - 548.82);
                }
            }
        }

        public static float calcularInss(float bruto)
        {
            if (bruto <= 868.29)
            {
                return bruto * 8 / 100;
            }
            else
            {
                if (bruto <= 1447.15)
                {
                    return bruto * 9 / 100;
                }
                else
                {
                    if (bruto <= 2894.28)
                    {
                        return bruto * 11 / 100;
                    }
                    else
                    {
                        return (float)318.37;
                    }
                }
            }
        }

        public static float calcularFgts(float bruto)
        {
            return bruto * 8 / 100;
        }

        public static float calcularSalarioLiquido(float bruto)
        {
            float inss,ir;
            inss = calcularIr(bruto);
            ir = calcularIr(bruto);
            return bruto - inss - ir;
        }
    }
}
