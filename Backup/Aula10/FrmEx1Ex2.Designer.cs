﻿namespace Aula10
{
    partial class FrmEx1Ex2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpPotenciacao = new System.Windows.Forms.GroupBox();
            this.txtExpoente = new System.Windows.Forms.TextBox();
            this.lblExpoente = new System.Windows.Forms.Label();
            this.txtBase = new System.Windows.Forms.TextBox();
            this.lblBase = new System.Windows.Forms.Label();
            this.txtResultado = new System.Windows.Forms.TextBox();
            this.lblResultado = new System.Windows.Forms.Label();
            this.btnCalcular = new System.Windows.Forms.Button();
            this.grpFatorial = new System.Windows.Forms.GroupBox();
            this.txtResFat = new System.Windows.Forms.TextBox();
            this.lblResFat = new System.Windows.Forms.Label();
            this.txtValor = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.btnCalcular1 = new System.Windows.Forms.Button();
            this.grpPotenciacao.SuspendLayout();
            this.grpFatorial.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpPotenciacao
            // 
            this.grpPotenciacao.Controls.Add(this.txtResultado);
            this.grpPotenciacao.Controls.Add(this.lblResultado);
            this.grpPotenciacao.Controls.Add(this.txtExpoente);
            this.grpPotenciacao.Controls.Add(this.lblExpoente);
            this.grpPotenciacao.Controls.Add(this.txtBase);
            this.grpPotenciacao.Controls.Add(this.lblBase);
            this.grpPotenciacao.Location = new System.Drawing.Point(16, 16);
            this.grpPotenciacao.Name = "grpPotenciacao";
            this.grpPotenciacao.Size = new System.Drawing.Size(152, 120);
            this.grpPotenciacao.TabIndex = 0;
            this.grpPotenciacao.TabStop = false;
            this.grpPotenciacao.Text = "Potenciação";
            // 
            // txtExpoente
            // 
            this.txtExpoente.Location = new System.Drawing.Point(76, 48);
            this.txtExpoente.Name = "txtExpoente";
            this.txtExpoente.Size = new System.Drawing.Size(52, 20);
            this.txtExpoente.TabIndex = 9;
            this.txtExpoente.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblExpoente
            // 
            this.lblExpoente.Location = new System.Drawing.Point(8, 48);
            this.lblExpoente.Name = "lblExpoente";
            this.lblExpoente.Size = new System.Drawing.Size(64, 20);
            this.lblExpoente.TabIndex = 8;
            this.lblExpoente.Text = "Expoente";
            this.lblExpoente.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtBase
            // 
            this.txtBase.Location = new System.Drawing.Point(76, 20);
            this.txtBase.Name = "txtBase";
            this.txtBase.Size = new System.Drawing.Size(52, 20);
            this.txtBase.TabIndex = 7;
            this.txtBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblBase
            // 
            this.lblBase.Location = new System.Drawing.Point(8, 20);
            this.lblBase.Name = "lblBase";
            this.lblBase.Size = new System.Drawing.Size(64, 20);
            this.lblBase.TabIndex = 6;
            this.lblBase.Text = "Base";
            this.lblBase.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtResultado
            // 
            this.txtResultado.Location = new System.Drawing.Point(76, 76);
            this.txtResultado.Name = "txtResultado";
            this.txtResultado.ReadOnly = true;
            this.txtResultado.Size = new System.Drawing.Size(52, 20);
            this.txtResultado.TabIndex = 11;
            this.txtResultado.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblResultado
            // 
            this.lblResultado.Location = new System.Drawing.Point(8, 76);
            this.lblResultado.Name = "lblResultado";
            this.lblResultado.Size = new System.Drawing.Size(64, 20);
            this.lblResultado.TabIndex = 10;
            this.lblResultado.Text = "Resultado";
            this.lblResultado.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCalcular
            // 
            this.btnCalcular.Location = new System.Drawing.Point(184, 112);
            this.btnCalcular.Name = "btnCalcular";
            this.btnCalcular.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular.TabIndex = 1;
            this.btnCalcular.Text = "&Calcular";
            this.btnCalcular.UseVisualStyleBackColor = true;
            this.btnCalcular.Click += new System.EventHandler(this.btnCalcular_Click);
            // 
            // grpFatorial
            // 
            this.grpFatorial.Controls.Add(this.txtResFat);
            this.grpFatorial.Controls.Add(this.lblResFat);
            this.grpFatorial.Controls.Add(this.txtValor);
            this.grpFatorial.Controls.Add(this.lblValor);
            this.grpFatorial.Location = new System.Drawing.Point(16, 148);
            this.grpFatorial.Name = "grpFatorial";
            this.grpFatorial.Size = new System.Drawing.Size(152, 120);
            this.grpFatorial.TabIndex = 2;
            this.grpFatorial.TabStop = false;
            this.grpFatorial.Text = "Fatorial";
            // 
            // txtResFat
            // 
            this.txtResFat.Location = new System.Drawing.Point(76, 48);
            this.txtResFat.Name = "txtResFat";
            this.txtResFat.ReadOnly = true;
            this.txtResFat.Size = new System.Drawing.Size(52, 20);
            this.txtResFat.TabIndex = 11;
            this.txtResFat.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblResFat
            // 
            this.lblResFat.Location = new System.Drawing.Point(8, 48);
            this.lblResFat.Name = "lblResFat";
            this.lblResFat.Size = new System.Drawing.Size(64, 20);
            this.lblResFat.TabIndex = 10;
            this.lblResFat.Text = "Resultado";
            this.lblResFat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtValor
            // 
            this.txtValor.Location = new System.Drawing.Point(76, 20);
            this.txtValor.Name = "txtValor";
            this.txtValor.Size = new System.Drawing.Size(52, 20);
            this.txtValor.TabIndex = 7;
            this.txtValor.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // lblValor
            // 
            this.lblValor.Location = new System.Drawing.Point(8, 20);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(64, 20);
            this.lblValor.TabIndex = 6;
            this.lblValor.Text = "Valor";
            this.lblValor.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnCalcular1
            // 
            this.btnCalcular1.Location = new System.Drawing.Point(184, 244);
            this.btnCalcular1.Name = "btnCalcular1";
            this.btnCalcular1.Size = new System.Drawing.Size(75, 23);
            this.btnCalcular1.TabIndex = 3;
            this.btnCalcular1.Text = "&Calcular";
            this.btnCalcular1.UseVisualStyleBackColor = true;
            this.btnCalcular1.Click += new System.EventHandler(this.btnCalcular1_Click);
            // 
            // FrmEx1Ex2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 290);
            this.Controls.Add(this.btnCalcular1);
            this.Controls.Add(this.grpFatorial);
            this.Controls.Add(this.btnCalcular);
            this.Controls.Add(this.grpPotenciacao);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmEx1Ex2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Exercícios 01 e 02";
            this.Load += new System.EventHandler(this.FrmEx1Ex2_Load);
            this.grpPotenciacao.ResumeLayout(false);
            this.grpPotenciacao.PerformLayout();
            this.grpFatorial.ResumeLayout(false);
            this.grpFatorial.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpPotenciacao;
        private System.Windows.Forms.TextBox txtExpoente;
        private System.Windows.Forms.Label lblExpoente;
        private System.Windows.Forms.TextBox txtBase;
        private System.Windows.Forms.Label lblBase;
        private System.Windows.Forms.TextBox txtResultado;
        private System.Windows.Forms.Label lblResultado;
        private System.Windows.Forms.Button btnCalcular;
        private System.Windows.Forms.GroupBox grpFatorial;
        private System.Windows.Forms.TextBox txtResFat;
        private System.Windows.Forms.Label lblResFat;
        private System.Windows.Forms.TextBox txtValor;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.Button btnCalcular1;
    }
}